local awful = require("awful")
local gears = require("gears")
local base_theme_file = awful.util.get_themes_dir() .. "default/theme.lua" 
local theme = gears.protected_call(dofile, base_theme_file)

theme.widget_border      = "#000000"

theme.fg_widget_cpu_high = "#FF5656"
theme.fg_widget_cpu_mid  = "#F6E30D"
theme.fg_widget_cpu_low  = "#AECF96"

-- https://github.com/streetturtle/awesome-wm-widget
theme.widget_main_color = "#74aeab"
theme.widget_red = "#e53935"
theme.widget_yelow = "#c0ca33"
theme.widget_green = "#43a047"
theme.widget_black = "#000000"
theme.widget_transparent = "#00000000"

return theme
